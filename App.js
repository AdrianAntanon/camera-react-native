/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import SplashScreen from './src/screens/SplashScreen';

const App = () => {

  return (
    <SplashScreen />
  );
};

export default App;
