import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/HomeScreen';
import CameraScreen from '../screens/CameraScreen';
import PhotoEditorScreen from '../screens/PhotoEditorScreen';
import ShareScreen from '../screens/ShareScreen';

import Colors from '../constants/Colors';

const CameraNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Camera: CameraScreen,
    PhotoEditor: PhotoEditorScreen,
    Share: ShareScreen
  }, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
    },
    headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary,
  }
});

export default createAppContainer(CameraNavigator);


