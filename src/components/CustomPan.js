import React, { useRef } from 'react';
import { StyleSheet, Animated, PanResponder, Image } from 'react-native';

const CustomPan = (props) => {
  const { current } = props;

  const pan = useRef(new Animated.ValueXY()).current;

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        pan.setOffset({
          x: pan.x._value,
          y: pan.y._value
        });
      },
      onPanResponderMove: Animated.event(
        [
          null,
          { dx: pan.x, dy: pan.y }
        ], { useNativeDriver: false }
      ),
      onPanResponderRelease: () => {
        pan.flattenOffset();
      }
    })
  ).current;

  return (
    <Animated.View
      style={{
        transform: [{ translateX: pan.x }, { translateY: pan.y }]
      }}
      {...panResponder.panHandlers}
    >
      <Image style={styles.icon} source={current} />

    </Animated.View>
  );
}

const styles = StyleSheet.create({
  icon: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  }
});

export default CustomPan;