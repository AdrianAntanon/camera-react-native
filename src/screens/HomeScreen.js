import React from 'react';
import { View, StyleSheet, Text, Button, Platform } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import HeaderButton from '../components/HeaderButton';

const HomeScreen = (props) => {
  return (
    <View style={styles.container} >
      <Text>Menú de inicio</Text>
    </View>
  );
}

HomeScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Home',
    headerRight: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title='Photo'
          iconName={Platform.OS === 'android' ? 'camera-outline' : 'camera'}
          onPress={() => {
            navData.navigation.navigate('Camera')
          }}
        />
      </HeaderButtons>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default HomeScreen;