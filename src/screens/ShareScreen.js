import React from 'react';
import { View, StyleSheet, Text, Button } from 'react-native';

const ShareScreen = (props) => {
  return (
    <View style={styles.container} >
      <Text>ShareScreen</Text>
      <Button
        title="Compartir"
        onPress={() => { console.log("Compartiendo foto..."); }}
      />
    </View>
  );
}

ShareScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Share'
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default ShareScreen;