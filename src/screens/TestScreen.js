import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const TestScreen = (props) => {
  return (
    <View style={styles.container} >
      <Text style={styles.title}>Pantalla de la cámara</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 20,
  },
});

export default TestScreen;