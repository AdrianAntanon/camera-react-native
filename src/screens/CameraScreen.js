import React, { useState } from 'react';
import { View, StyleSheet, Button, Text, PermissionsAndroid, TouchableOpacity, Image } from 'react-native';
import { RNCamera } from 'react-native-camera';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons'
import Logo from '../assets/logo-nexia-transparente.png';


const PendingView = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'lightgreen',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Text>Waiting</Text>
  </View>
);

const CameraScreen = (props) => {
  const [cameraType, setCameratype] = useState(RNCamera.Constants.Type.front);

  const { navigation } = props;

  const takePicture = async function (camera) {
    const options = { quality: 0.5, base64: true };
    const data = await camera.takePictureAsync(options);

    (() => {
      navigation.navigate('PhotoEditor', {
        otherParam: data.uri
      });
    })()
  };

  const changeCamera = () => {
    if (cameraType === 1) setCameratype(RNCamera.Constants.Type.back)
    else setCameratype(RNCamera.Constants.Type.front)
  }

  return (
    <View style={styles.container} >
      <RNCamera
        style={styles.preview}
        type={cameraType}
        flashMode={RNCamera.Constants.FlashMode.off}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
      >
        {({ camera, status }) => {
          if (status !== 'READY') return <PendingView />;
          return (
            <View style={styles.captureContainer}>
              <Image style={{ width: '100%', height: 25, resizeMode: 'center' }} source={Logo} />
              <TouchableOpacity onPress={() => changeCamera()}>
                <FontAwesomeIcon icon={faSyncAlt} style={styles.cameraChange} size={30} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => takePicture(camera)} style={styles.capture} />
            </View>
          );
        }}
      </RNCamera>
    </View>
  );
}

CameraScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Camera'
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

export default CameraScreen;