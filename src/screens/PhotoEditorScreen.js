import React, { useState } from 'react';
import { View, StyleSheet, Platform, Image, ScrollView, TouchableOpacity } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import HeaderButton from '../components/HeaderButton';


import CustomPan from '../components/CustomPan';

const iconsUris = [
  require('../assets/icons/logo-batman.png'),
  require('../assets/icons/joker-batman.png'),
  require('../assets/icons/bane-batman.png'),
  require('../assets/icons/batman.png'),
  require('../assets/icons/skull.png'),
  require('../assets/icons/excellent.png'),
];

const PhotoEditorScreen = (props) => {
  const [currentIcon, setCurrentIcon] = useState([])

  const imageUri = props.navigation.state.params.otherParam;

  const handleIcon = () => {
    setCurrentIcon();
  }

  return (
    <View style={styles.container} >
      <Image
        source={{ uri: imageUri }}
        style={styles.imgback}
      />

      {currentIcon.map((current, index) => {
        return (
          <CustomPan
            key={index.toString()}
            current={current}
          />
        );
      })}

      <ScrollView horizontal={true} style={styles.scroll}>
        {iconsUris.map((img, index) => {
          return (
            <TouchableOpacity
              key={index.toString()}
              onPress={() => setCurrentIcon(prevState => prevState.concat(img))}
            >
              <Image
                style={styles.icon}
                source={img}
              />
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
}

PhotoEditorScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Photo Editor',
    headerRight: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title='Photo'
          iconName={Platform.OS === 'android' ? 'share-outline' : 'share'}
          onPress={() => {
            navData.navigation.navigate('Share');
          }}
        />
      </HeaderButtons>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: '100%',
    width: '100%',
  },
  imgback: {
    position: 'absolute',
    height: '80%',
    width: '100%',
    top: 0,
  },
  scroll: {
    position: 'absolute',
    bottom: 0,
    height: '20%',
    width: '100%',
    backgroundColor: 'black',
    padding: 20,
  },
  icon: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
});

export default PhotoEditorScreen;