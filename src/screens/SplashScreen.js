import React, { useState, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import AnimatedSplash from "react-native-animated-splash-screen";

import Logo from '../assets/logo-nexia-transparente.png';
import CameraNavigator from '../navigation/CameraNavigator';
import HomeScreen from './HomeScreen';

const SplashScreen = (props) => {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => setIsLoaded(true), 1500);
    return () => clearTimeout(timer);
  }, [])

  return (
    <AnimatedSplash
      translucent={true}
      isLoaded={isLoaded}
      logoImage={Logo}
      backgroundColor={"#262626"}
      logoHeight={220}
      logoWidth={220}
    >
      <CameraNavigator />
    </AnimatedSplash>
  );
}

const styles = StyleSheet.create({
  container: {

  }
});

export default SplashScreen;